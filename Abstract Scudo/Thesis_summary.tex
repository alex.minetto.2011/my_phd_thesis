\documentclass{article} % For LaTeX2e
\usepackage{nips15submit_e,times}
\usepackage[hidelinks]{hyperref}
\usepackage{tikz}
\usepackage{tabularx}
\usepackage{cite}
\usepackage{url}
\usepackage[psamsfonts]{amssymb}
\newcommand{\argmin}{\arg\!\min}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{mathtools}
\usetikzlibrary{patterns}
\usepackage{caption}
\usepackage{subcaption}
%\documentstyle[nips14submit_09,times,art10]{article} % For LaTeX 2.09


%\title{ \textsc{{\normalsize Global Navigation Satellites Systems and related applications}}\\{\LARGE Research Abstract on Cooperative Positioning\\}}

\title{\LARGE GNSS-only Collaborative Positioning Methods for Networked Receivers\\}

%\author{Alex Minetto ID: 211419}
%\And Advisor\\ Prof. Roberto Garello \\ roberto.garello@polito.it \And Co-Advisor\\ Prof. Monica Visintin \\ monica.visintin@polito.it}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

%\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}

\begin{minipage}{.2\textwidth}
\includegraphics[width=2.4cm]{Politecnico_di_Torino_-_Logo.png}
\end{minipage}
\begin{minipage}{.8\textwidth} 
{\Large \hspace{.3cm} \textsc{POLITECNICO DI TORINO}}\\
\hspace{-2cm} \noindent\rule{\textwidth}{0.2pt}\\[.2cm]
${ }$ ${ }$ ${ }$ ${ }$ ${ }$ {\textsc{{\small PhD in Electrical, Electronics and Communications Engineering}}}\\[.1cm]
${ }$ ${ }$ ${ }$ ${ }$ ${ }$\textbf{Alex Minetto} - \textit{alex.minetto@polito.it}\\[.1cm]
${ }$ ${ }$ ${ }$ ${ }$ ${ }$PhD Student - XXXII cycle
\end{minipage}
\vspace{.2cm}

\maketitle
\vspace{-2.2cm}

%{\footnotesize \textit{It is not knowledge, but the act of learning, not possession but the act of getting there, which grants the greatest enjoyment.} Carl F. Gauss, Letter to Bolyai (1808)}

\section*{Summary}

The Global Navigation Satellite System (GNSS) has been mostly considered as an ubiquitous and reliable positioning and navigation technology characterized nowadays by considerable precision and accuracy but weakened by critical limitations due to the intrinsic nature of radio navigation systems. Indeed, {GNSS} positioning in harsh environment has been intended as one of the major challenges in the field of Positioning and Navigation Technologies (PNT) due to several impairments affecting the quality of the received signals, thus the positioning estimation performed by the receivers. The quality of {GNSS} absolute positioning can be enhanced through the integration of further information about the state of a given agent (i.e. position, velocity, heading) or exploiting additional data from the surrounding environment, which can be directly obtained through sensors  (i.e. {UWB}, Lidar, Sonar), or through the network connectivity. 
In parallel with the advances in satellite-based and network-based augmentation systems, sensor fusion and complementary positioning systems, in the last decades, Cooperative Positioning (CP) has been addressed as a further paradigm for improving localization of networked users relying on the exchange of independent information. The basic approach has leveraged the estimation of relative measurements and the sharing of these data through ad-hoc communication channels or permanent network infrastructures. A key aspect in the cooperative positioning of swarm of agents is the computation of inter-agent ranges, which can be used as auxiliary information to mutually improve localization performance. Most of the literature on the topic provides integration solution of existing radio/visual ranging technologies. Although such hardware-based solutions are effective for the purpose, a set of research contributions has shown that the exchange of {GNSS} measurements and navigation data can be considered to estimate terrestrial collaborative range measurements between networked receivers.
In light of this, this work considers the exploitation of redundant visible satellites among interconnected users (i.e. generic agents equipped with networked {GNSS} receiver) as a resource for the improvement of {GNSS}-based solutions. Despite the correlation among the measurements, such a redundancy can be effectively exploited to determine relative distances between receivers through opportunistic usage of the network connectivity, thus leading to the \textit{collaborative estimation} of the {baseline length}. 
The proposed approach has been investigated to overcome the Line-of-Sight limitation in the sensor-based sensing of the surrounding environment through network-based measurements among connected {GNSS} users. Therefore, in the proposed framework the focus is on collaborative techniques, which uniquely exploit the exchange of raw GNSS data, without the need of additional sensors. 
The problem of a profitable usage and integration of redundant and correlated inter-agent information computed within a network of {GNSS} receiver has been named \textit{{GNSS}-only {CP}}. 
 A set of techniques has been hence investigated to determine the distance between cooperating agents by exchanging {GNSS}-only data and applying differential estimation algorithms to solve for the inter-vehicle distance. The state-of-the-art methods for range estimation through {GNSS} observables were investigated in this work. Such techniques are based on the evaluation of the modulus of a displacement vector between pairs of cooperating agents. Due to the nature of the problem, the algorithms require three or more equations thus, three or more shared satellites. Since this condition cannot be easily satisfied in dense urban scenarios, a less demanding collaborative strategy was proposed to overcome the limited availability of shareable satellites. A novel method, named Inter-agent Range (IAR), is proposed to cope with limited availability of shareable satellites. In fact, it relies on the cooperation between pairs of {GNSS} receivers exploiting even one only shared satellite. Due to the asynchronous measurements sets specific solutions were tailored to compensate for time inconsistency of data introduced by the intrinsic misalignment between the signal reception times and by the possible latency injected by communication channel. %Multiple inter-agent measurements can be further used to improve the estimation of the distance w.r.t. a collaborating agent before it is integrated in a further navigation algorithm. 
The quality of the inter-agent measurements directly affects the performance of the positioning computation in a potential hybrid localization algorithm (i.e. legacy satellite pseudoranges and terrestrial ranges are used). The measurement can be tightly integrated through Least Mean Square-based algorithm for Position-Time-Velocity solution, which is suitable for static applications. Alternatively, the integration can be afforded by a set of popular techniques for range integration in positioning algorithm in dynamic scenarios (i.e. Extended Kalman Filter (EKF), Unscented Kalman Filter (UKF), Particle Filter).
In light of this, this thesis aims at investigating the paradigm of {GNSS}-based {CP} according to a bottom-up approach: from the theoretical bounds of a \textit{hybridized tight integration} of collaborative measurements up to the development of a real-time Proof-of-Concept (PoC) for networked mobile devices equipped with ultra-low-cost {GNSS} receivers. First a proper framework was defined for the analysis and investigation of {GNSS}-based ranging among networked receivers. Then the paradigm of {GNSS}-based {CP} is explored through numerical and controlled-environment simulations, and eventually, the {PoC} is presented along with an on-field analysis of feasibility and a performance assessment of the technology.  
This study addresses the concept of networked {GNSS} receivers being able to share {GNSS} measurements to enable 
enhanced positioning and navigation capabilities. The theoretical limits of such a collaborative {DGNSS} were investigated up to demonstrate that the exchange of pseudorange measurements represents a source of information for networked receivers. This thesis investigated the proposed {CP} paradigm by looking mostly at the position estimation. Such a vector quantity is surely the most appealing for Location Based Services (LBSs) and GNSS receivers in general. However, the approach can be extended to the velocity estimation of cooperating kinematic agents exploiting collaborative differential velocity computation through independent, correlated Doppler measurements.
Such a solution could be effective to reduce the dependency of the velocity determination to sensors and odometers, thus presenting the same advantages of GNSS-only enhanced positioning. Doppler measurements are considerably more stable both for high-end and mass market receivers and this aspect could lead to outstanding performance. 

According to the results presented in this thesis by means of numerical simulation, and assessed through realistic simulations and through real-time implementation, a proof of the benefits led by cooperative enhancement in {GNSS} positioning can be remarked, even in case of ultra low-cost receivers. Among the potential extensions of this work addressing a real implementation of the paradigm, it is worthy to mention the development of multi-constellation and multi-frequency algorithms for improved inter-agent distance estimation, the analysis of the geometrical displacement of both satellites and agents through the estimation of the precision bound for correlated/uncorrelated ranging information and the development of advanced filtering of the raw measurements exchanged among the agents. Despite the clear contributions provided towards improved GNSS-only positioning and navigation, the proposed framework, CAPS.loc, can open a variety of possibilities for centralized/distributed real-time processing of {GNSS} raw measurements in different class of receivers. Among the possible applications we can mention collaborative anti-spoofing techniques based on outliers detection, a collector of big-data about monitoring of ionospheric indexes or for real-time mapping of the quality of {GNSS} measurements in harsh context. Furthermore, the implementation of this {CP} paradigm provided a baseline for further scientific investigation on state-of-the art cooperative algorithm inherited from different research and application fields which could support a sustainable {GNSS} navigation in the near future of PNTs.  



\begin{flushright}
\textbf{Alex Minetto}\\{\scriptsize NavSAS Research Group}
\end{flushright}




\end{document}
