% !TEX root = ../toptesi-scudo-example.tex
% !TEX encoding = UTF-8 Unicode
%*****************************************************************
%*********************** Third chapter ***************************
%*****************************************************************

\chapter{Introduction}
\label{ch:introduction}

% ********************** Define Graphics Path ********************
    \graphicspath{{Chapter0/Figures}}



Up to 10 billions of electronic devices with localization capabilities will populate the world by 2025 to satisfy the booming request for \ac{LBS}\cite{gsa2019market}. While professional \ac{GNSS} receivers will keep dominating the market of highly-precise, accurate and reliable positioning and navigation, mass market products will demand for smart solutions to obtain the best performance at the minimum cost. A considerable number of \ac{LBS} such as vehicle pooling, vehicle sharing and remote control of \ac{UAV}s are mostly based on the positiong solution provided by mobile mass market devices (e.g. smartphones) \cite{7471482}.

In the last decades a remarkable research effort has been broadly spent in the field of navigation and positioning by mostly addressing sensor integration to \ac{GNSS} \cite{grewal2007global,4770175}. As depicted in Figure \ref{fig:refArchi}, a growing set of subsystems is nowadays responsible of the provisioning of localization and navigation data.  As an example, modern vehicular navigation systems can exploit proprioceptive sensors such as \ac{INS} to improve the quality of absolute positioning solutions \cite{jekeli2012inertial,noureldin2012fundamentals}. These components are devoted to the measure of values internal to the system (e.g. angular rate,
wheel rotations, accelerations). \ac{GNSS}/\ac{INS} integration schemes benefit from the accurate measurements provided by \ac{INS} and periodically compensate for their typical drifts through \ac{GNSS} solutions. Such an approach, known as \textit{dead reckoning} is conceived on top of absolute positioning, thus position estimation based on loose sensors integration can always benefits from improved \ac{GNSS} performance \cite{5290369}. Advanced integration schemes such as tight and ultra-tight \ac{GNSS}/\ac{INS} offer promising performance in harsh environment where standalone \ac{GNSS} is typically weak \cite{s17020255,7993222,8248655,cristodaro2019advanced} but they require reliable sensors and challanging implementations such as in mobile devices where they still show relevant issues \cite{7247673,8460352}. 
In fact, in many devices such as smartphones and smartwatches or in limiting conditions (e.g. off-road driving, urban canyon, forest), the integration of \ac{INS} can be challenging due to the uncorrect modelling of the dynamics of the object itself \cite{rodriguez2018robust,cherif2018loosely}. 

In parallel, the sensing of the surrounding environment has become a relevant aid to the navigation and it can be accomplished through cameras ot through several exteroceptive sensors such as \ac{UWB}, LiDAR and many others. Indeed, when inertial sensing is not available or sufficiently reliable, other kinds of measurements can be used to retrieve information about the surrounding environment such as the distances from anchor points whose position is known a-priori \cite{8255823,8272318,5466205} or by considering peers as anchors of opportunity relying on their position estimates \cite{7297658}. Therefore, the contribution of exteroceptive sensors to improve absolute and relative localization has been deeply explored in literature \cite{7015602}.For instance, \ac{CCD}/\ac{CMOS} camera can be used as a complementary heading sensor through the computation of vanishing points \cite{cristodaro2019advanced}.
%Several contributions about \ac{GNSS}/\ac{INS} integration in smartphones used proper supports to keep it fixed with a known heading w.r.t. the body frame of the target vehicles. 


Although \ac{GNSS} is a fundamental source for geolocalization, the applications related to relative positioning were tipically referred in literature and real implementations to carrier-phase ambiguity resolution \cite{basnayake2011relative,1709862} for \ac{RTK} and \ac{DGNSS} applications. 

Hovewer, recent advances in communication networks are going to enable the integration of such relative measurements among connected \ac{GNSS} receivers thus addressing the field of \ac{CP}. \ac{CP} gathers several techniques through wich a set of agents (also referred as nodes in network domain) requiring for a positioning solution collaborate with others to determine their own location. The paradigm of \ac{CP} also known in literature as \textit{network localization}, \textit{cooperative localization} or \textit{collaborative localization} has been hence addressed to guarantee positioning capabilities to specific nodes which may be not equipped with positioning systems or connectivity to other nodes within the same network \cite{zekavat2011handbook}. Furthermore most of the early contributions addressed \ac{CP} to provide positioning and navigation in \ac{GNSS}-denied environment and they were mostly focused on sensor networks. A misleading classification was attributed in the field of \ac{GNSS} to peer-to-peer \ac{AGNSS} solutions.

A deep investigation on the optimality of \ac{CP} for network applications has been also provided in \cite{shen2010limitsPart2,5871406}. The interest in \ac{CP} strategies was first raised in the field of robotics by several contributions \cite{1668252,4285854,1067998},then leading to Simultaneous Localization and Mapping \cite{7434174,5937274}. Later, other works faced the problem of localization in wireless sensor networks \cite{shen2017dsrc} and afterwards towards vehicular navigation \cite{hossain2016cooperative,7857839,alam2013cooperative}. A novel approach to range-only localization has been also proposed in \cite{uluskan2018circular} where terrestrial range measurements retrieved from occasional anchors allow to approximate the position estimate. The most of the contributions related to these fields concerned \ac{MLE} approaches which is not suitable to exploit prior knowledge about the dynamics of the receivers effectively. Differently, another approach named \textit{belief propagation} has been deeply explored in the field of indoor positioning contemplating mostly static or low dynamic agents \cite{8374808,caceres2010hybrid}. 

As an example, interesting results about the superiority of collaborative strategies with respect to Differential \ac{GPS} techniques \cite{6236174,6020821,minetto2018tight,Minetto:2019:GCP:3331054.3331552} have been presented. This trend justified the fusion of \ac{DGNSS} and collaborative navigation techniques for a further improvement of the performance \cite{7015602,948650} oriented to the future generation of \ac{GNSS} receivers.


Previous works investigated the computation of distance among \ac{GNSS} receivers. For example in \cite{6240332}, the authors presented a technique to compute inter-vehicular range measurements through weighted \ac{GNSS} double differences. . 

Pioneering works on the tight integration of \ac{DGPS} and \ac{INS} in an \ac{EKF}-based positioning algorithm were proposed to improve accuracy and availability of \ac{GPS} positioning \cite{7015602,948650,4343975}.
A two-steps positioning algorithm has been proposed to refine the \ac{GNSS}-only position estimates through a \ac{ML} approach constraining the positioning solution by means of inter-vehicle ranges obtained by weighted double differentiation of shareable pseudorange measurements \cite{liu2014improving}. The approach foresees a distributed computation of the locations of all the agents by each agent. A final estimated was then performed as a weighted mean of the different estimates provided by the collaborating agents. The scheme proposed in \cite{liu2014improving} does not exploit the dynamics of the agents, thus being suboptimal in dynamics applications. Recent works such as \cite{lassoued2019cooperative,8688455} demonstrate how timely and appealing is \ac{CP} in the field of \ac{GNSS}.

According to the taxonomy presented in \cite{zekavat2011handbook} the proposed architecture was developed as \textit{distance-based}, \textit{distributed}, \textit{sequential} and \textit{probabilistic} positioning estimation. First we addressed the cooperative determination of the distance among the receivers, namely inter-agent distance to provide additional information to the positioning problem. Such a computation is conceived to be carried out by each agent independently in a distributed fashion. The collaborative position update of each receiver is not used as a reference for other receiver to avoid error propagation and the estimation is locally performed with a probabilistic Bayesian approach which guarantees also the estimation of uncertainty on the output. 
%

\begin{figure}
\centering
\includegraphics[width=0.9\columnwidth,trim={0cm 0cm 0cm 0cm},clip]{/ReceiverScheme.png}
\caption{{\small Reference architecture of a modern positioning system providing data channel for positioning applications.}}
\label{fig:refArchi}
\end{figure} 



\section{Research motivation and objectives}

Provided the fundamental role of positioning and navigation technologies and the growing attention to the new generation of telecommunication networks, 
this thesis aim at developing an innovative framework for the cooperation of networked \ac{GNSS} receiver for the improvement of positioning and navigation performance. The algorithms which will be presented in the context of the proposed \ac{GNSS}-based \ac{CP}, are conceived to exploit \ac{GNSS} measurements as they are provided by professional as well as by Ultra-low Cost receivers \ac{GNSS}. It comes out that the proposed framework can be intended as an auxiliary layer which can be implemented in any receivers architecture and it can be adapted to different grades of \ac{PNT}, from high-end products to smartphones.  Although the aforementioned works has brought significant contribution to the integration of auxiliary measurements, they were conceived in more general frameworks. The proposed study is tightly linked to \ac{GNSS} and it exploits natively both satellite-to-user range and Doppler measurements for the improvement of the positioning in kinematics systems. 

Therefore, this thesis aims in parallel at defying theoretical aspects about the use of non independent measurements in the positioning estimation. The identification of a theoretical background is addressed to the cooperation of \ac{GNSS} receiver sharing \ac{GNSS} measurements for the improvement of their positioning estimation. Provided that the proposed paradigm is conceived on top of \ac{GNSS} as a refinement of \ac{GNSS}-only positioning solutions, sensor fusion will as well as network differential corrections will not be investigated in this work. This work addresses the computation and the analysis of distance measurements among peer receivers and their integration in the computation of the receiver status and the position, specifically.


\section{Main Contributions}

The main contributions of this thesis can be summarized as follows:

\begin{itemize}
\item Definition of the  problem of \ac{GNSS}-based cooperative positioning according to the hybridized architecture present in literature. 
\item Analysis, development and performance assessment of state-of-the art differential ranging algorithms for the collaborative estimation of the inter-agent distances. 
\item Definition and development of a theoretical framework for of a novel method for the estimation of the baseline length in limited visibility conditions.
\item Study and design of a tight-integration scheme for Bayesian estimation integrating correlated measurements.
\item Definition, study and implementation of the concept of \ac{GNSS} networked receiver through asynchronous instances of a \ac{GNSS} software receiver.
\item Real-signal simulation test campaigns through professional signal generator
\item Design and development of a smartphone-based mass-market \ac{PoC} of \ac{GNSS}-based \ac{CP}.
\item Analysis of the feasibility and effectiveness of the framework through 4G network carried out through on-field test campaigns of the proof-of-concept.
\end{itemize}

Part of the work included in this thesis was presented through several contributions in international conferences renown in \ac{GNSS} community  \cite{MinettoIAReusipco,minettoCRLB,minetto2018tight,minetto2018PLANS,Minetto:2019:GCP:3331054.3331552,gogoiVTC2019,minetto2017IAR} and peer-reviewed journal \cite{8926529,Gogoi_2018}. A side work concerning positioning investigation under geomagnetic storm was also published in further peer-reviewed journal \cite{linty2018effects}. Some contribution were instead part of the HANSEL \ac{ESA} project deliverables and technical reports \cite{hanselD1,hanselD2,hanselD3}. Eventually, further side contributions were published about \ac{GNSS} anomalies in magazines \cite{SVN49,GalileoOutage}. \\

Some research contributions included in this work were acknowledged by the international \ac{GNSS} community with a Best Student Paper Award at the 32nd Internation Technical Meeting of the Satellite Navigation Division of the Institute of Navigation (ION GNSS+ 2019)\cite{minettoCRLB} and with the Francesco Carassa Award 2018 by Gruppo Telecomunicazione e Tecnologie dell'Informazione (GTTI). Furthermore, the innovative cooperative framework developed within the HANSEL project won the Italian Prize of the Galileo Master sponsored by the Italian Space Agency, was selected among the finalists of the University Challenge and reached the top ten of the Galileo Masters 2019 in Helsinki.

\section{Outline of the thesis}

\textbf{Chapter \ref{ch:GNSSfundamentals}} first provides an overview on radio-positioning systems among which \ac{GNSS} is described in detail. It describes the overall system architecture and the basic theoretical principles which allow positioning solutions and navigation.

\textbf{Chapter \ref{ch:rxarchitecture}} describes the architecture of a \ac{GNSS} receiver by providing the fundamental knowledge and definitions for the computation of \ac{GNSS} raw measurements. Eventually, the concept architecture of a networked \ac{GNSS} receiver for the implementation of \ac{CP} is discussed as a guideline of the proposed research.

\textbf{Chapter \ref{ch:rangingAlgorithms}} presents the differential techniques at the state of the art for the estimation of the distance among two receivers through \ac{GNSS} measurements, namely \ac{GNSS}-based ranging methods. It also introduces a novel method, named \ac{IAR} to determine the same measurements in non-ideal satellite visibility conditions. 

\textbf{Chapter \ref{ch:GNSSrangingAssessment}} presents a detailed investigation about the methods presented in Chapter \ref{ch:rangingAlgorithms}. A formal assessment of the proposed \ac{IAR} is discussed through a proper theoretical framework and experimental results are presented to assess the theoretical behaviour. A comparison of the different \ac{GNSS}-based ranging techniques is eventually provided by means of simulated \ac{GNSS} signal.

\textbf{Chapter \ref{ch:navFilters}} first recalls a set of popular navigation filters conceived for linearized and approximated Bayesian positioning estimation. The hybridization of the filters is proposed  proposed and eventually the analysis of theoretical bounds are investigated for the exploitation of collaborative measurements.

\textbf{Chapter \ref{ch:theoreticalResults}} gives a detailed discussion about implementation aspects related to \ac{GNSS}-based \ac{CP}. Different application are presented and the benefits of the approach are discussed in terms of accuracy and precision of the positioning solution. Eventually a trade-off analysis is also provided by comparing the computational complexity and accuracy of two Bayesian filters.

\textbf{Chapter \ref{ch:proofOfConcept}} presents a \ac{PoC} of \ac{GNSS}-based cooperative positioning implemented on Android Smartphones. The context of the project which funded the research is first introduced. Afterwards, a background on hardware and software is reported. Eventually the system architecture is explained in detail and feasibility and effectiveness of the proposed paradigm are discussed through on-field tests. Selected results are discussed in details while the whole test campaign is commented according to a given set of performance metrics.

\textbf{Chapter \ref{ch:conclusions}} aim at summarizing the research presented in this thesis and it provides a discussion about future activities about the topic. \\
\textbf{Appendixes} recall a set of theoretical aspects fundamental to the complete understanding of the proposed approach.