% !TEX root = ../toptesi-scudo-example.tex
% !TEX encoding = UTF-8 Unicode
% ******************************* Thesis Appendix A 

\chapter{Position Error Covariance Matrix and Geometrical representations} 
\label{appendix:covEllipse}

\graphicspath{{Appendix1/Figures/Chapter}}

The position uncertainty associated to the output of an unbiased estimator, such as a \ac{WLS}, describes the \textit{precision} of the estimate itself. The scope of this appendix is to provide the basic knowledge for the computation and visualization of \textit{position error ellipses} representing estimated or measured covariance matrices of the positioning solution in simulation environment. The content of this appendix has been rearranged from \cite{errEllipse01,errEllipse02} to support visualization tools used within this research. 

\section{Sample Covariance Matrix estimation}

The \textit{sample covariance matrix} at a given time instant $t_k$ is by definition a $N \times N$ matrix $\mathbf{P}_k =\left[p_{ij}\right]$ modelling the probability distribution of a given multi-variate random variable such as the estimated state vector of a \ac{GNSS} receiver, $\bm{\theta}_k={\begin{bmatrix} {\theta}_{1}& {\theta}_{2}&\dots & {\theta}_{N}\end{bmatrix}}^\mathrm{T}$. By fixing a discrete time instant $t_k$ for the sake of readability, the entries of $\mathbf{P}_k$ are defined as

\begin{equation}
p_{ij}=\frac{1}{W-1}\sum_{w=1}^{W}\left(  {\theta}_{i}^{(w)}-\bar{\bm{\theta}}_i \right)  \left({\theta}_{j}^{(w)}-\bar{\bm{\theta}}_j \right)
\end{equation}

where  ${\theta}_{i}^{(w)}$ is the $w$-th observation of the $i$-th random variable belonging to $\bm{\theta}_k$ and $\bar{\bm{\theta}}_i$ is the mean value of the same $i$-th random variable over $W$ observations. Therefore, $p_{ij}$ is an estimate of the covariance between the $i$-th variable and the $j$-th variable of the considered multi-variate random variable, $\bm{\theta}_k$. In terms of the observation vectors, an equivalent formulation of the sample covariance is

\begin{equation}
\mathbf{P}_k ={1 \over {W-1}}\sum _{w=1}^{W}(\mathbf{\bm{\theta}}^{(w)}-\mathbf{\bar {\bm{\theta}}})(\mathbf {\bm{\theta}}^{(w)}-\mathbf{\bar {\bm{\theta}}} )^{\mathrm {T}}
\end{equation}

Alternatively, arranging the observation vectors as the columns of a matrix,

\begin{equation}
\mathbf{\Theta}_k ={\begin{bmatrix} \bm{\theta}^{(1)}&\bm{\theta}^{(2)}&\dots &\bm{\theta}^{(W)}\end{bmatrix}}
\end{equation}

which is a matrix of $N$ rows and $W$ columns, the sample covariance matrix can be computed as

\begin{equation}
\mathbf{P}_k ={\frac {1}{W-1}}(\bm{\Theta}_k - \bar{ \bm{\theta}} \,\mathbf {1} _{W}^{\mathrm {T} })(\mathbf{\Theta}_k - \bar{\bm{\theta}} \,\mathbf{1} _{W}^{\mathrm {T} })^{\mathrm {T} }
\end{equation}

where $\mathbf{1}_{N}$ is an $W \times 1$ vector of ones. If the observations are arranged as rows instead of columns, so $\bar{\bm{\theta}}$  is now a $1\times N$ row vector and $\mathbf{M} ={\bm{\Theta}}^{\mathrm{T}}$ is an $W\times N$ matrix whose column $j$ is the vector of $W$ observations on variable $j$, then applying transposes operators

\begin{equation}
\mathbf{P}_k ={\frac {1}{W-1}}(\mathbf{M}_k -\mathbf {1}_{W}\mathbf {{\bar {x}}^{\mathrm {T} }} )^{\mathrm {T} }(\mathbf {M}_k -\mathbf {1}_{W}\mathbf {{\bar{x}}^{\mathrm {T} }})
\end{equation}

Like covariance matrices for random vector, sample covariance matrices are positive semi-definite. To prove it, note that for any matrix $\mathbf {A}$  the matrix  $\mathbf {A}^{T}\mathbf {A} $ is positive semi-definite. Furthermore, a covariance matrix is positive definite if and only if the rank of the ${\theta}_{i}-\bm{\bar {\theta}}$ vectors is $N$.

\section{Covariance Matrix and Error Ellipse }

For a given set of applications such as road positioning navigation, horizontal precision models the \ac{HPE} which is considered a fundamental \ac{KPI} in \ac{GNSS} \cite{van1998innovation}. For bivariate observations indeed, a valuable method to investigate the precision of the positioning solution is the use of \textit{error ellipses} a.k.a. \textit{information ellipses} in Information Theory or \textit{confidence ellipses} in statistics. An error ellipse represents an \textit{iso-contour} of a Gaussian distribution, and allows you to visualize a bi-dimensional confidence interval. The limitation to a 2D Cartesian reference system does not constitute a loss of generality, since the following derivations can be arbitrarily extended to multi-variate random variable by extending the number of degrees of freedom, as well. Let us remark that error ellipse representation is rarely meaningful for non-spatial coordinates, therefore in the following, only geometrical coordinates will be considered.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.8\textwidth,clip=true,trim={0.1cm 0.1cm 0.1cm 0.1cm}]{/AlignedEllipse.eps}
  \caption{\small{Error ellipse of uncorrelated 2D positioning unbiased solutions drawn for 95\% of the confidence interval.}}
    \label{fig:covAlignedExample}
\end{figure}

Figure \ref{fig:covAlignedExample} shows a $95$\% confidence ellipse for a set of uncorrelated, bivariate normally distributed positioning solutions. This confidence ellipse defines the region that contains $95$\% of all the samples (red dots) that can be drawn from a zero-mean Gaussian distribution with covariance equal to 

\begin{equation}
\mathbf{P}_k=\begin{bmatrix}
\sigma_x^2 & \sigma_{xy} \\
\sigma_{yx} & \sigma_y^2
\end{bmatrix}
\label{eq:alignedCov}
\end{equation}

where $\sigma_x^2=3$ and $\sigma_y^2=1$ and $\sigma_{xy}=\sigma_{yx}=0$. 

It is evident that the magnitudes of the ellipse axes depend on the variance of the data. The largest variance is in fact in the direction of the $x$-axis, whereas the smallest variance lies in the direction of the $y$-axis.

The generic equation of an axis-aligned ellipse with a major axis of length $2a$ and a minor axis of length $2b$, centered at the origin, is defined by the following equation:

\begin{equation*} 
\left(\frac{ x } { a }\right)^2 + \left(\frac{ y } { b }\right)^2 = 1 
\end{equation*}

In our case, the length of the axes are defined by the standard deviations $\sigma_x$ and $\sigma_y$ of the data such that the equation of the error ellipse becomes:

\begin{equation}  
\left(\frac{ x } { \sigma_x }\right)^2 + \left(\frac{ y } { \sigma_y }\right)^2 = s 
\label{eq:ellipse}
\end{equation}

where $s$ defines the scale of the ellipse and could be any arbitrary number (e.g. s=1). The question is now how to choose $s$, such that the scale of the resulting ellipse represents a chosen confidence level (e.g. a 95\% confidence level corresponds to $s=5.991$).

Our 2D data is sampled from a multivariate Gaussian with zero covariance. This means that both the $x$-values and the $y$-values are normally distributed too. Therefore, the left hand side of equation \eqref{eq:ellipse} actually represents the sum of squares of independent normally distributed data samples. The sum of squared Gaussian data points is known to be distributed according to a Chi-Square distribution. A Chi-Square distribution is defined in terms of \textit{degrees of freedom}, which represent the number of unknowns. In the case of bivariate data there are two unknowns, thus two degrees of freedom.

Therefore, we can easily obtain the probability that the above sum, and thus $s$ equals a specific value by calculating the Chi-Square likelihood. In fact, since we are interested in a confidence interval, we are looking for the probability that $s$ is less then or equal to a specific value which can easily be obtained using the cumulative Chi-Square distribution, such that to collect the 95\% of probability, the following holds

\begin{equation*} 
P(s < 5.991) = 1-0.05 = 0.95 
\end{equation*}

Formally, a $95$ \% confidence interval corresponds to $s=5.991$. In other words, 95\% of the data will fall inside an ellipse defined as:

\begin{equation*} 
\left(\frac{ x } { \sigma_x }\right)^2 + \left(\frac{ y } { \sigma_y }\right)^2 = 5.991 
\end{equation*}

Similarly, a 99\% confidence interval corresponds to $s=9.210$ and a 90\% confidence interval corresponds to $s=4.605$.

The error ellipse shown in Figure \ref{fig:covAlignedExample} can be drawn as an ellipse with a major axis length equal to $2\sigma_x$ $\sqrt{5.991}$ and the minor axis length to $2\sigma_y$ $\sqrt{5.991}$.

Exploiting the decomposition of the covariance matrix through its eigenvalues and eigenvector it is possible to visualize arbitrary confidence intervals of our solution, thus to visually evaluate the precision of the estimated solution for a generic covariance matrix.

\subsection{Eigendecomposition of Covariance}

As shown in Figure \ref{fig:covAlignedExample}, $\mathbf{P}_k$ defines both the \textit{spread} (variance), and the \textit{orientation} (covariance) of the observation. It is of interest to find the vector directed into the direction of the largest spread of the data, and whose magnitude equals the spread (variance) in this direction for an arbitrarily correlated set of data.

When the covariance terms of $\mathbf{P}_k$ are not null, the resulting error ellipse will not be aligned to the Cartesian axis. The previous considerations hold only if we consider a new reference system in which the ellipses can be actually aligned to the axis.

Instead of computing the variance along the $y$ and $x$ axis , we aim at determining the variance along the major and minor axis of the error ellipse, as shown by red and black arrows in Figure \ref{fig:covExample}. A covariance matrix can be considered as a linear transformation which scales and rotates a set of originally uncorrelated data. In light of this, the directions of the axis of the error ellipse are still defined by the eigenvectors of such a transformation matrix. Thus, the $95$\% confidence ellipse can be defined similarly to the axis-aligned case, with the major axis of length $2\sqrt{5.991 \lambda_1}$ and the minor axis of length $2\sqrt{5.991 \lambda_2}$, where $\lambda_1$ and $\lambda_2$ represent the eigenvalues of the covariance matrix.

It is hence possible to draw a covariance ellipse representing the covariance matrix of uncorrelated data and aftwerwards applying a rotation according to the angle

\begin{equation*} 
\alpha = \arctan \frac{\mathbf{v}_1(y)}{\mathbf{v}_1(x)}
\end{equation*}

which is the angle of the largest eigenvector towards the x-axis and $\mathbf{v}_1$ is the eigenvector of the covariance matrix that corresponds to the largest eigenvalue. The angle $\alpha$ determines the rotation of the ellipse according to the true statistical properties of the observations.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.8\textwidth,clip=true,trim={0.1cm 0.1cm 0.1cm 0.1cm}]{/GenericEllipses.eps}
  \caption{\small{Error ellipses of correlated 2D positioning solutions shown for 5 confidence intervals indicated in the legend. Eigenvector are multiplied by the respective eigenvalue with a factor $2$ to reach $95$\% confidence value.}}
    \label{fig:covExample}
\end{figure}

Based on the minor and major axis lengths and the angle $\alpha$ between the major axis and the x-axis, it becomes trivial to plot the confidence ellipse as shown in Figure \ref{fig:covExample}. 

Let us suppose to compute a set of point  $\begin{bmatrix} \mathbf{x} & \mathbf{y} \end{bmatrix}^\top$ describing \eqref{eq:ellipse}, the ellipse modelling the covariance matrix of a generic dataset can be draw by means of 

\begin{equation}
\begin{bmatrix} \mathbf{x}' \\ \mathbf{y}' \end{bmatrix}=  \begin{bmatrix}
\cos(\alpha) & -\sin(\alpha) \\
\sin(\alpha) & \cos(\alpha)
\end{bmatrix} \begin{bmatrix} \mathbf{x} \\ \mathbf{y} \end{bmatrix}  \begin{bmatrix}
\cos(\alpha) & -\sin(\alpha) \\
\sin(\alpha) & \cos(\alpha)
\end{bmatrix}^\top
\label{eq:ellipseRot}
\end{equation}

Summarizing, a generic 2D error ellipse can be computed according to the following pseudo-code

\begin{algorithm}[H]
\floatname{algorithm}{Error ellipse}
\renewcommand{\thealgorithm}{}
\caption{Pseudocode for the generation of error ellipses}
\label{alg:ellipseAlgorithm}
\begin{algorithmic}[1]
\STATE Calculate the eigenvectors and eigenvalues through the eigendecomposition of the covariance matrix \\
\STATE Get the largest eigenvalue \\
\STATE Compute the confidence intervals according to the number of degrees of freedom.\\
\STATE Draw the ellipse in $x$ and $y$ coordinates according to \eqref{eq:ellipse}.\\
\STATE Define a rotation matrix $\mathbf{R}$.\\
\STATE Rotate the ellipse according to \eqref{eq:ellipseRot}.
\end{algorithmic}
\end{algorithm} 
















%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%By defining such vector as $\bm{v}$, then the projection of a set of data $\mathbf{\Theta}_k$ onto this vector is obtained as $\bm{v}^{\top}\mathbf{\Theta}_k$, and the variance of the projected data is $\bm{v}^{\top}\mathbf{P}_k \bm{v}$. Since we are looking for the vector $\bm{v}$ that points into the direction of the largest variance, the components must be chosen in order to maximize $\bm{v}^{\top} \mathbf{P}_k \bm{v}$. Maximizing any function of the form $\bm{v}^{\top} \mathbf{P}_k \bm{v}$ with respect to $\bm{v}$, where $\bm{v}$ is a normalized unit vector, can be formulated as a so called \textit{Rayleigh quotient}. The maximum of such a Rayleigh quotient is obtained by setting $\bm{v}$ equal to the \textit{largest eigenvector} of matrix $\mathbf{P}_k$.
%
%By considering $||\bm{v}||=\lambda$ and $\bm{\lambda}$ as the vector of the eigenvalues of the covariance matrix, the largest \textit{eigenvector} $\bm{v}$ such that $\max_{\bm{\lambda}}(\lambda)$ always points into the direction of the largest variance of the observations, and the magnitude of this vector, $||\bm{v}||$ ,equals the corresponding \textit{eigenvalue}. The second largest eigenvector is always orthogonal to the largest eigenvector, and points into the direction of the second largest spread of the data.

%Figure \ref{fig:covAlignedExample} illustrates that the angle of the ellipse w.r.t. the Cartesian reference frame is determined by $\mathbf{P}_k$. In this case, the covariance terms are equal to zero, such that the data is uncorrelated, resulting in an axis-aligned error ellipse.
%
%
%
%The magnitudes of the axes depend on the variance of the data. In the example the largest variance is in the direction of the $x$-axis, whereas the smallest variance lies in the direction of the $y$-axis.
%
%In general, the equation of an ellipse aligned to the Cartesian reference system with a major axis of length $2a$ and a minor axis of length $2b$, centered at the origin, is defined by the following equation:
%
%\begin{equation}
%\left(\frac{x}{a}\right)+\left(\frac{y}{b}\right) = 1
%\end{equation}
%
%where s defines the scale of the ellipse and could be any arbitrary number (e.g. $s$=1). The question is now how to choose $s$, such that the scale of the resulting ellipse represents a chosen confidence level (e.g. a 95\% confidence level corresponds to $s=5.991$).
%
%Our 2D data is sampled from a multivariate Gaussian with zero covariance. This means that both the x-values and the y-values are normally distributed too. Therefore, the left hand side of equation actually represents the sum of squares of independent normally distributed data samples. The sum of squared Gaussian data points is known to be distributed according to a so called Chi-Square distribution. A Chi-Square distribution is defined in terms of degrees of freedom, which represent the number of unknowns. In our case there are two unknowns, and therefore two degrees of freedom.
%
%Therefore, we can easily obtain the probability that the above sum, and thus s equals a specific value by calculating the Chi-Square likelihood. In fact, since we are interested in a confidence interval, we are looking for the probability that s is less then or equal to a specific value which can easily be obtained using the cumulative Chi-Square distribution.


%In cases where the data is not uncorrelated, such that a covariance exists and the corresponding covariance matrix is not diagonal (as in \eqref{eq:alignedCov} for Figure \ref{fig:covAlignedExample}, the resulting error ellipse will not be aligned w.r.t. the Cartesian axis. In this case, the reasoning of the above paragraph only holds if we temporarily define a new coordinate system such that the ellipse becomes axis-aligned, and then rotate the resulting ellipse afterwards.
%
%In other words, whereas we calculated the axial variances $\sigma_x$ and $\sigma_y$ parallel to the x-axis and y-axis earlier, we now need to calculate these variances parallel to what will become the major and minor axis of the confidence ellipse. The directions in which these variances need to be calculated are shown in Figure \ref{fig:covExample}.



%These directions are actually the directions in which the data varies the most, and are defined by $\mathbf{P}_k$. The covariance matrix can be considered as a matrix that linearly transformed some uncorrelated observations to obtain the currently observed data. Indeed, the vectors shown by red and black vectors in Figure \ref{fig:covExample}, are the eigenvectors of the covariance matrix of the data, whereas the length of the vectors corresponds to the square root of the eigenvalues, $\bm{\lambda}$.
%
%The eigenvalues therefore represent the spread of the data in the direction of the eigenvectors. In other words, the eigenvalues represent the variance of the data in the direction of the eigenvectors. In the case of axis aligned error ellipses, i.e. when the covariance equals zero, the eigenvalues equal the variances of the covariance matrix and the eigenvectors are equal to the definition of the x-axis and y-axis. In the case of arbitrary correlated data, the eigenvectors represent the direction of the largest spread of the data, whereas the eigenvalues, $\bm{\lambda}$, define how large this spread really is.
%
%As we saw earlier, we can represent the covariance matrix by its eigenvectors and eigenvalues:
%
%\begin{equation*}  
%\mathbf{P}_k \mathbf{v} = \lambda \mathbf{v} 
%\end{equation*}
%
%where $\bm{v}$ is an eigenvector of $\mathbf{P}_k$, and $\lambda=||\bm{v}||$ is the corresponding eigenvalue.
%
%Equation holds for each eigenvector-eigenvalue pair of matrix $\Sigma$. In the 2D case, we obtain two eigenvectors and two eigenvalues. The system of two equations defined by equation can be represented efficiently using matrix notation:
%
%\begin{equation*}  
%\mathbf{P}_k \, \mathbf{V} = \mathbf{V} \, \mathbf{P}_k\mathbf{L}
%\end{equation*}
%
%where $\mathbf{V}$ is the matrix whose columns are the eigenvectors of $\mathbf{P}_k$ and $\mathbf{L}$ is the diagonal matrix whose non-zero elements are the corresponding eigenvalues. This means that we can represent the covariance matrix as a function of its eigenvectors and eigenvalues:
%
%(15) \begin{equation*}  
%\mathbf{P}_k = \mathbf{V} \, \mathbf{L} \, \mathbf{V}^{-1} 
%\end{equation*}
%
%Equation is called the eigendecomposition of the covariance matrix and can be obtained using a Singular Value Decomposition algorithm. Whereas the eigenvectors represent the directions of the largest variance of the data, the eigenvalues represent the magnitude of this variance in those directions. In other words, V represents a rotation matrix, while $\sqrt{\mathbf{L}}$ represents a scaling matrix. The covariance matrix can thus be decomposed further as:
%
%\begin{equation*}
%\mathbf{P}_k = \mathbf{R} \, \mathbf{S} \, \mathbf{S} \, \mathbf{R}^{-1} 
%\end{equation*}
%
%where $\mathbf{R}=\mathbf{V}$ is a rotation matrix and $\mathbf{S}=\sqrt{\mathbf{L}}$ is a scaling matrix.
%
%In equation (6) we defined a linear transformation $\mathbf{T}=\mathbf{R} \, \mathbf{S}$. Since $\mathbf{S}$ is a diagonal scaling matrix, $\mathbf{S} = \mathbf{S}^{\intercal}$. Furthermore, since $\mathbf{R}$ is an orthogonal matrix, $\mathbf{R}^{-1} = \mathbf{R}^{\top}$. Therefore, $\mathbf{T}^{\intercal} = (\mathbf{R} \, \mathbf{S})^{\top} = \mathbf{S}^{\top} \, \mathbf{R}^{\top} = \mathbf{S} \, \mathbf{R}^{-1}$. The covariance matrix can thus be written as:
%
%\begin{equation*}  
%\mathbf{P}_k' = \mathbf{R} \, \mathbf{S} \, \mathbf{S} \, \mathbf{R}^{-1} = \mathbf{T} \, \mathbf{T}^{\top},
%\end{equation*}
%
%In other words, if we apply the linear transformation defined by $\mathbf{T}=\mathbf{R} \, \mathbf{S}$ to the original white data $\bm{\Theta}$ shown by Figure 7, we obtain the rotated and scaled data $\bm{\Theta}'$ with covariance matrix $\mathbf{T} \, \mathbf{T}^{\top} = \mathbf{P}_k' = \mathbf{R} \, \mathbf{S} \, \mathbf{S} \, \mathbf{R}^{-1}$. 

\subsection{Covariance conversion between reference frames}

When the estimation of a covariance matrix is provided in \ac{ECEF} coordinates, the visualization of the confidence interval in terms of error ellipses does not provide useful information about its geometrical distribution on a local frame (e.g. \ac{ENU}). A conversion of the 3D covariance matrix must be performed to project the 2D error ellipse to the local reference frame.

According to Appendix \ref{appendix:referenceFrame}, the conversion of a set of positioning observations between different reference frame can be performed applying a proper rotation matrix as

\begin{equation}
\bm{\theta}_k' = \mathbf{R} \bm{\theta}_k.
\end{equation}

According to the following derivation, the same rotation matrix can be used to perform the conversion $\mathbf{P}_k\to \mathbf{P}_k'$ \cite{soler1985transformation}. 

\begin{proof}
\begin{equation}
\begin{split}
\mathbf{P}_k'  & =  \text{E}(\bm{\theta}'\bm{\theta}^{'\top})- \text{E} \left( \bm{\theta}' \right) \text{E}\left( \bm{\theta}^{'\top} \right)\\
& = \text{E}(\mathbf{R}\bm{\theta} \bm{\theta}^{\top}  \mathbf{R}^{\top}) - \text{E}(\mathbf{R}\bm{\theta})\text{E}(\bm{\theta}^{\top} \mathbf{R}^{\top}) \\
& = \mathbf{R} \text{E}(\bm{\theta} \bm{\theta}^{\top}) \mathbf{R}^{\top} -  \mathbf{R} \text{E} \bm{\theta}\text{E}(\bm{\theta}^{\top})\mathbf{R}^{\top}\\
& = \mathbf{R}( \text{E}(\bm{\theta} \bm{\theta}^{\top}) - \text{E}(\bm{\theta}) \text{E}(\bm{\theta}^{\top}))\bm{\theta}^{\top} \\
& = \mathbf{R}\mathbf{P}_k\mathbf{R}^{\top}
\end{split}
\end{equation}
\end{proof}

%			 = & \text{E}(\mathbf{R}\theta\theta^{\top} \mathbf{R}^{\top})−\text{E}(\mathbf{R}\theta)E(\theta^{\top} \mathbf{R}^{\top)} \\
%		     = & \mathbf{R} E(\theta\theta^{\top}) \mathbf{R}^{\top}−\mathbf{R}\text{E}(\theta)\text{E}(\theta^\top)\mathbf{R}^{\top} \\
%		     = & R( E(\theta\theta^\top)−E(\theta)E(\theta^\top))\theta^\top
%		     = & \mathbf{R}\mathbf{P}_k\mathbf{R}^\top

This transformation is particularly needed when sample covariance cannot be computed through of the observations such as in \ac{PF}-based estimation but is directly estimated from the navigation filter (e.g. \ac{KF}-based Bayesian estimation) in a different reference frame w.r.t. to the one of interest.
Once the conversion $\mathbf{P}_k\to \mathbf{P}_k'$ is performed, the eigendecomposition of $\mathbf{P}_k'$ can be used to draw covariance ellipse in the new reference frame.


%According to Appendix \ref{sec:conversion}, the conversion between \ac{ECEF} and \ac{ENU} reference frames is performed to linear transformation. This principle holds for the conversion of $\mathbf{P}_k$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%\textbf{APPENDICE}
%The popular law of cosines, a.k.a. Carnot Theorem of triangles, relates the values of the three side lengths and one angle in a triangle
%
%\begin{equation}
%\label{eq:carnotTheorem}
%c^2=a^2+b^2-2ab \cos \gamma
%\end{equation}
%
%where $a$,$b$ and $c$ are the lengths of the sides and $\gamma$ is the angle included between the first two, as shown in picture [\textbf{FIG}].
%
%
%
%According to the knowledge of three of the aforementioned elements, the equation for a side or an angle can be solved.
%Equation \eqref{eq:carnotTheorem} can be straightforwardly implemented in floating-point arithmetic but it can produce very inaccurate results whether is used under certain conditions. If the investigated triangle is highly acute, numerical results show high relative errors due to catastrophic cancellation in the subtraction of very similar quantities. This issue, known as \textit{loss of significance} is highly impacting when large distance measurements are involved in  \eqref{eq:carnotTheorem}. 
%By rearranging \eqref{eq:carnotTheorem} it is possible to compute the value of $c$ according to
%
%\begin{equation}
%\label{eq:carnotTheoremSide}
%c=\sqrt{a^2 + b^2 -2ab \cos \gamma}
%\end{equation}
%
%given that $a \approx b$ have comparable magnitudes and angle $\gamma$ is severely close to zero, then $\gamma \approx 1$ \cite{754809}.
%
%This is the case of terrestrial \ac{IAR} computation based on satellites-to-receivers range measurements whose order of magnitude can easily reach 6 order of magnitude w.r.t. the baseline length.  
%
%In practical terms
%
%\begin{equation}
%\label{eq:limitCarnot}
%a^2 + b^2 -2ab \cos \gamma \simeq  a^2 + a^2 -2a^2 \cos(0) \simeq 2a^2-2a^2 \simeq 0
%\end{equation}
%
%even if $c=10^{-20}$  can be still represented in floating points arithmetic. 
%A possible solution to the numerical cancellation is to exploit the Taylor expansion of the cosine for angle $\gamma$ close to $0$
%
%\begin{align}
%\label{eq:carnotExpansion}
%c^2 = a^2 + b^2 -2ab \cos \gamma & = a^2 + b^2 -2ab(1+cos \gamma -1)\\
% 						   & = \left( a^2 + b^2 -2ab \right) -2ab(cosC - 1)\\
%						   & = \left( a^2 -2ab + b^2 \right) -2ab \left( -\frac{\gamma^2}{2!} + \frac{\gamma^4}{4!} + o(\gamma^4) \right)\\
%						   & \simeq \left(a-b \right)^2+2ab \left( \frac{\gamma^2}{2!} + \frac{\gamma^4}{4!}\right)\\
%\end{align}
%
%a stable formula to prevent numerical cancellation is then
%
%\begin{equation}
%\label{eq:iarStable}
%c = \sqrt{\left( a - b\right)^2 + ab\gamma \left(1 - \frac{\gamma^2}{12} \right)}
%\end{equation}
%
%In \eqref{eq:iarStable} both the terms of the sum are positive and error cancellation is only possible for the term $(a-b)$, which turns to be critical only in case of zero-baseline tests. In general the noise affecting the measurements of the sides mitigates cancellation phenomena.
%
