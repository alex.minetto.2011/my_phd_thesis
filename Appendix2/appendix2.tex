% !TEX root = ../toptesi-scudo-example.tex
% !TEX encoding = UTF-8 Unicode
% ******************************* Thesis Appendix B 


\chapter{Fundamentals on Reference Systems and Frames}
\label{appendix:referenceFrame}

\graphicspath{{Appendix2/Figures}}


All the satellite-based navigation systems such as \ac{GNSS} require the definition of a common reference frame to locate both satellite coordinates and user receivers. It is worthy to remark the difference among \textit{reference system} and \textit{reference frame}: the first is theoretically defined according to a standard model while the latter is an empirical implementation based on observations and further reference coordinates. The rigorous definition of the reference frames used in \ac{GNSS} is out of the scope of this work and further details on the topic can be found in theory books on geodesy or summarized in \cite{refFrame,refFrame2}. Therefore, the scope of this appendix is to recall the fundamentals about the conversion among different frames and their definitions.

\section{Conventional Celestial Reference System CRS}

The CRS coordinate system adopts the Earth's center of mass as the origin of a Cartesian reference frame. The fundamental plane corresponds to the average equatorial plane calculated at J2000.0 \footnote{J2000.0 or J2000 indicates 12h00 of 1st January 2000 of Gregorian Calendar (UT)}. The system is also known as Earth-Centered Inertial (ECI) and it is represented in Figure \ref{fig:ECI_onLLH} by considering the following mapping of the corresponding Cartesian orthogonal components:

\begin{itemize}
\item[] $\mathbf{z}_\ti{CRS}$: axis crossing the average geographic north pole
\item[] $\mathbf{x}_\ti{CRS}$: average vernal equinox
\item[] $\mathbf{y}_\ti{CRS}$: coordinate axis with respect to $\mathbf{z}_\ti{CRS}$ and $\mathbf{x}_\ti{CRS}$
\end{itemize}

Actually, the CRS is quasi-inertial reference frame since it is characterized by the annual revolution, thus by the accelerated motion of the Earth w.r.t. the Sun. 

\section{Conventional Terrestrial Reference System (TRS)}

The system is often referred as Earth Centered Earth Fixed (ECEF) with origin in the center of mass of the Earth and a fundamental plane corresponding to equatorial plane. Despite CRS/ECI, the coordinate axis system is coherent with Earth's daily rotation, that is to say it is not inertial. The fundamental plane contains the origin and it is perpendicular to the Conventional Terrestrial Pole (CTP). The $\mathbf{z}_{ECEF}$ axis is the intersection point between equator and Greenwich meridian as shown in Figure \ref{fig:TRS}. Axis convention is hence reported:

 \begin{itemize}
\item[] $\mathbf{x}_\ti{TRS}$: axis crossing the intersection between the equator and the Greenwich meridian
\item[] $\mathbf{z}_\ti{TRS}$: axis passing through the CTP
\item[] $\mathbf{y}_\ti{TRS}$: coordinate axis w.r.t. $\mathbf{x}_\ti{TRS}$ and $\mathbf{y}_\ti{TRS}$
\end{itemize}


\begin{figure*}[!h]
    \centering
    \begin{subfigure}[b]{0.6\columnwidth}
\includegraphics[width=\textwidth,trim={0.0cm 0.0cm 0.0cm 0.0cm},clip]{/CRSECI.pdf}
\caption{{\small CRS Earth Centered Inertial (ECI)s.}}
\label{fig:ECI_onLLH}
    \end{subfigure}
    \begin{subfigure}[b]{0.6\columnwidth}
\includegraphics[width=\textwidth,clip=true,trim={0 0 0 1cm}]{//TRSECEF.pdf}
\caption{Earth Centered Earth Fixed reference frame.}
 \label{fig:ECEF}
    \end{subfigure}
    \caption{{\small Coordinates Reference Systems.}}
    \label{fig:refs}
\end{figure*}

The main \acp{GNSS} adopt different reference frames:

\begin{itemize}
\item GPS: World Geodetic System WGS-84 (US \ac{DoD})\cite{WGS84}. 
\item GLONASS: Parametry Zemli 1990 (Parameters of the Earth 1990) (PZ-90).
\item GALILEO: Galileo Terrestrial Reference Frame (GTRF)(GeoForschungsZentrum Potsdam)(IGS).
\end{itemize}

\section{Conversion between reference systems}
\label{sec:conversion}

A conversion of the \ac{ECEF} coordinates is required to visualize the position in terms of latitude and longitude. For each coordinate pair, the height of a point is defined w.r.t. the geoid and a relative given model (e.g. WGS84 for \ac{GPS}). Such coordinates are referred as \ac{LLA} or LLH and they represent the usual coordinate system adopted to identify the position of a point on the Earth, namely through its \textit{ellipsoidal coordinates}.  

\paragraph{Ellipsoidal Coordinates ($\phi$, $\lambda$, $h$) } The Cartesian \ac{ECEF} can be mapped on an ellipsoidal model. The conversion is performed through the following transformation, according to Figure \ref{fig:LLH}

\begin{figure}[hb!]
  \centering
  \includegraphics[width=0.6\textwidth,clip=true,trim={0.1cm 0.1cm 0.1cm 0.1cm}]{/RSECEFWG84.pdf}
  \caption{\small{Ellipsoidal Coordinates \ac{LLA}/LLH.}}
    \label{fig:LLH}
\end{figure}


\begin{equation}
\label{eq:LLH2ECEF}
\begin{array}{lll}\displaystyle
\mathbf{x}_\ti{TRS}=(N+h)\cos(\phi)\cos(\lambda) &  \\[.1cm]
\mathbf{y}_\ti{TRS}=(N+h)\cos(\phi)\sin(\lambda) &  \\[.1cm]
\mathbf{z}_\ti{TRS}=((1-e^2)N+h)\sin(\phi) &  \\
\end{array}
\end{equation}

Where $N$ is the curvature radius along the meridians by varying $\phi$:

\begin{equation}
N=\frac{a}{\sqrt{1-e^2\sin^2\phi}}
\end{equation}

and $e$ is the eccentricity of the ellipsoid (often referred as \textit{stretching factor}) 

\begin{equation}
e^2=\frac{a^2-b^2}{a^2}=2f-f^2
\end{equation}

where $a$ and $b$ are the semi-major and the semi-minor axis respectively. The eccentricity can also be expressed in function of flattening coefficient $f$.

Inverse conversion from Ellipsoidal Coordinate System to \ac{ECEF} can be approximated by iterative solution. 

\paragraph{Conversion between ECEF to Local Tangent Plane (LTP) Coordinates } LTP coordinate system usually referred as East, North and Up (ENU) coordinates is widely adopted in GNSS terrestrial receiver and is suitable for the sought application. The \ac{ENU} coordinates are derived from \ac{ECEF}, thus the oblate ellipsoidal configuration is still valid. The axis are described as follows:

\begin{itemize}
\itemsep0em
\item[] $\mathbf{N}$: is the axes (coordinated to the tangent plane) which points towards north Pole identified by $\mathbf{z}_{TRS}$.
\item[] $\mathbf{E}$: it corresponds to the east axis and is aligned to the local parallel
\item[] $\mathbf{U}$: it is the normal vector of the LTP
\end{itemize}

In order to perform the conversion between ECEF to ENU and viceversa, two linear transformation are required. They correspond to two rotations of a Cartian reference system. 

\begin{enumerate}
\itemsep0em
\item Rotation of $90\deg-\phi$ on E-axis to align U-axis with $\mathbf{z}_{TRS}$ $\mathbf{R_1[-(\pi/2 -\phi)]}$
\item Rotation of $90\deg+\lambda$ on U-axis to align $\mathbf{x}_{TRS}$ to E-axis. $\mathbf{R_3[-(\pi/2+\lambda)]}$
\end{enumerate}

where $R_1$, $R_2$ and $R_3$ are defined as follows:

\begin{equation}
\mathbf{R_1}(\theta)=\left[
\begin{array}{ccc}
1 & 0 & 0 \\
0 & \cos\theta & \sin\theta \\
0 & -\sin\theta & \cos\theta
\end{array}
\right],
\mathbf{R_2}(\theta)=\left[
\begin{array}{ccc}
\cos\phi & 0 & -\sin\phi \\
0 & 1 & 0 \\
sin\theta & 0 & \cos\theta
\end{array}
\right],
\mathbf{R_3}(\theta)=\left[
\begin{array}{ccc}
\cos\theta & \sin\theta & 0 \\
-\sin\theta & \cos\theta & 0 \\
0 & 0 & 1
\end{array}
\right]
\end{equation}

The conversion in matrix form is expressed as:

\begin{equation}
\label{eq:conversion}
\left[ 
\begin{array}{ccc}
x  \\
y \\
z 
\end{array}
\right]=
\mathbf{R_1}[-(\pi/2+\lambda)]\mathbf{R_3}[-(\pi/2-\phi)]
\cdot
\left[ 
\begin{array}{ccc}
E  \\
N \\
U 
\end{array}
\right]
\end{equation}

computing the overall transformation matrix

\begin{equation}
\label{eq:transformation}
\mathbf{R_c}=\mathbf{R_1}[-(\pi/2+\lambda)]\mathbf{R_3}[-(\pi/2-\phi)]=\left[
\begin{array}{ccc}
-\sin\lambda & -\cos\lambda\sin\phi & cos\lambda\cos\phi \\
\cos\lambda & -sin\lambda\sin\phi & \sin\lambda\cos\phi \\
0 & \cos\phi & \sin\phi
\end{array}
\right]
\end{equation}

By substituting Equation \eqref{eq:transformation} in matrix Equation \eqref{eq:conversion} the coordinates are hence obtained as follows: 

\begin{equation}
\label{eq:axisENU}
\begin{array}{lll}\displaystyle
\mathbf{\hat{e}}=(-\sin\lambda, \cos\lambda,0) &  \\[.1cm]
\mathbf{\hat{n}}=(-\cos\lambda\sin\phi, -\sin\lambda\sin\phi,\cos\phi) &  \\[.1cm]
\mathbf{\hat{u}}=(\cos\lambda\cos\phi, \sin\lambda\cos\phi, \sin\phi) &  \\
\end{array}
\end{equation}

By applying trivial properties from elementary algebra $R_i^{-1}(\alpha)=R_i(-\alpha)=R_i^T(\alpha)$ it is possible to obtain the reverse relation as follows: 

\subsection{Range, Elevation and Azimuth computation from ENU frame}

Azimuth and elevation are relevant to describe the satellite relative observation from the receiver perspective. From a set of coordinates defined in \ac{ENU} reference frame, the \textit{range versor} is defined as follows:

\begin{equation}
\hat{\rho}=\frac{r^{sat}-r_{rcv}}{||r^{sat}-r_{rcv}||}
\end{equation}

where $r^{sat}$ and $r_{rcv}$ are the geocentric satellite and receiver coordinates respectively. Azimuth and elevation can be computed from \ac{LTP} reference by applying elementary trigonometric rules, according to Figure \ref{fig:LTP}

\begin{equation}
\label{eq:axisENU}
\begin{array}{lll}\displaystyle
\hat\rho \cdot \hat{e} = \cos E \sin A & \\[.1cm]
\hat\rho \cdot \hat{n} = \cos E \cos A &  \\[.1cm]
\hat\rho \cdot \hat{u} = \sin E  &  \\
\end{array}
\end{equation}

By inverting the previous equations:

\begin{equation}
\label{eq:axisENU}
\begin{array}{lll}\displaystyle
E = \arcsin (\hat{\rho} \cdot \hat{u}) & \\[.1cm]
A = \arctan \left( \frac{\hat{\rho} \cdot \hat{e}}{\hat{\rho} \cdot \hat{n}} \right) &  \\[.1cm]
\end{array}
\end{equation}

Let us remark that if $\lambda$ and $\phi$ are \textit{ellipsoidal coordinates} hence, the vector $\hat{u}$ is orthogonal to the \ac{LTP} and to the reference ellipsoid. Otherwise, if they are in \textit{spherical coordinates} the same plane is tangent to a sphere.


\begin{figure*}[!h]
    \centering
    \begin{subfigure}[b]{0.49\columnwidth}
\includegraphics[width=\textwidth,trim={0.0cm 0.0cm 0.0cm 0.0cm},clip]{/LTPECEF.pdf}
\caption{{\small \ac{LTP} shown in \ac{LLA} system.}}
\label{fig:LTP_onLLH}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\columnwidth}
\includegraphics[width=\textwidth,clip=true,trim={0 0 0 1cm}]{/LTPENU.pdf}
\caption{LTP Coordinates Reference}
 \label{fig:TRS}
    \end{subfigure}
    \caption{{\small \ac{LTP} shown w.r.t. a \ac{LLA} frame and as reference frame}.}
    \label{fig:LTP}
\end{figure*}




%\begin{figure}[h!]
%%  \begin{subfigure}[b]{0.50\textwidth}
%%   \includegraphics[width=\textwidth,clip=true,trim={0 0 0 1cm}]{/figures/LTPECEF.pdf}
%%      \caption{LTP visualization in LLH system}
%%    \label{fig:CRS}
%%
%%
%%  \end{subfigure}
%%  %\hfill
%%  \begin{subfigure}[b]{0.5\textwidth}
%%    \includegraphics[width=\textwidth,clip=true,trim={0 0 0 1cm}]{/figures/LTPENU.pdf}
%%    \caption{LTP Coordinates Reference}
%%    \label{fig:TRS}
%%      \end{subfigure}
%%  \caption{\textit{Coordinates Reference Systems}}
%%  
%\end{figure}

%\chapter{Self-Adaptive Iterative Algorithm for robust integration}
%
%The hybrid PVT algorithm presented, leads indeed to a typical ill-conditioned set of equations and it shows high instability of the convergence of the solution. An iterative algorithm proposed in \cite{Deng2015453} is adopted to enhance the convergence performance avoiding the inversion of the ill-conditioned $\mathbf{H}\mathbf{H}'$ product by means of a Self Adaptive Iterative Algorithm (SAIA) of Weighted Least Mean Square. For sake of completeness, the core steps of SAIA method are remarked here with a more familiar notation. As a general assumption, all the measurements (i.e. pseudoranges, IARs) are affected by Gaussian distributed errors as reasonably stated in the previous section. 
%
%\[
%  \left\{\def\arraystretch{1.2}%
%  \begin{array}{@{}c@{\quad}l@{}}
%    N=\mathbf{H}_i^T\mathbf{H}_i+\Lambda I
%    \\W=\mathbf{H}_i'Y+\Lambda \mathbf{x}(k-1)     \end{array}\right.
%\]
%
%
%The perturbation parameter $\Lambda$ is iteratively determined to accelerate the convergence. It must satisfy the condition $0 \leq \Lambda \leq 1$ :
%
%\[
%  \left\{\def\arraystretch{1.2}%
%  \begin{array}{@{}c@{\quad}l@{}}
%    \lambda=min(|eig(\mathbf{H}_i^T \mathbf{H}_i)|)
%     \\\Lambda=\lambda\cdot 10^{0.5| log_{10} (\lambda)|+1}      \end{array}\right.
%\]
%
%$Z$ is calculated given the upper triangular matrix C obtained from the Cholesky decomposition of $\mathbf{H}_i$
%
%\begin{equation}
%CZ=W
%\end{equation}
%
% and then $\mathbf{x}$ is calculated solving the problem
%
%\begin{equation}
%C^T\mathbf{x}=Z
%\end{equation}
%
%It has to be remarked that the solution $\mathbf{x}$ is a differential vector with respect to the preceding approximation point and hence for each receiver $\hat{\mathbf{x}}_i=\mathbf{x}_0-\mathbf{x}$.\\

