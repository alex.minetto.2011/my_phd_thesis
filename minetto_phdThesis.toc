\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {italian}{}
\babel@toc {italian}{}
\babel@toc {italian}{}
\babel@toc {english}{}
\contentsline {chapter}{List of Tables}{xii}{chapter*.4}% 
\contentsline {chapter}{List of Figures}{xiv}{chapter*.5}% 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Research motivation and objectives}{3}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Main Contributions}{4}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Outline of the thesis}{4}{section.1.3}% 
\contentsline {chapter}{\numberline {2}Global Navigation Satellite Systems and Cooperative Positioning}{7}{chapter.2}% 
\contentsline {section}{\numberline {2.1}From the Observation of the Stars to Radio Navigation}{7}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Brief history of navigation technologies}{7}{subsection.2.1.1}% 
\contentsline {subsubsection}{Geodesy, timekeeping and astronomy}{8}{subsection.2.1.1}% 
\contentsline {subsubsection}{Terrestrial Radionavigation System}{8}{subsection.2.1.1}% 
\contentsline {paragraph}{\ac {TDoA}}{9}{subsection.2.1.1}% 
\contentsline {paragraph}{\ac {DoA}}{9}{figure.caption.7}% 
\contentsline {paragraph}{Doppler Navigation}{9}{figure.caption.8}% 
\contentsline {paragraph}{\ac {ToA}}{9}{figure.caption.8}% 
\contentsline {subsubsection}{Early Satellite Navigation Systems}{10}{figure.caption.9}% 
\contentsline {section}{\numberline {2.2}Navigation Satellite Systems}{11}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}GPS}{11}{subsection.2.2.1}% 
\contentsline {subsection}{\numberline {2.2.2}Galileo}{13}{subsection.2.2.2}% 
\contentsline {subsection}{\numberline {2.2.3}Other global and regional systems}{13}{subsection.2.2.3}% 
\contentsline {subsection}{\numberline {2.2.4}Augmentation Systems and \ac {DGPS}}{14}{subsection.2.2.4}% 
\contentsline {section}{\numberline {2.3}Fundamental of PVT computation in GNSS}{15}{section.2.3}% 
\contentsline {subsection}{\numberline {2.3.1}Determination of the velocity}{17}{subsection.2.3.1}% 
\contentsline {chapter}{\numberline {3}Receiver architecture and satellite-to-receiver measurements}{21}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Signal structure and frequency plans}{21}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Receiver Architecture}{26}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Received signal}{26}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}Front-end}{26}{subsection.3.2.2}% 
\contentsline {subsection}{\numberline {3.2.3}Acquisition}{28}{subsection.3.2.3}% 
\contentsline {subsection}{\numberline {3.2.4}Tracking}{30}{subsection.3.2.4}% 
\contentsline {subsubsection}{Code tracking loops}{30}{figure.caption.19}% 
\contentsline {subsubsection}{Carrier tracking loops}{31}{figure.caption.21}% 
\contentsline {subsection}{\numberline {3.2.5}Navigation Message demodulation}{34}{subsection.3.2.5}% 
\contentsline {section}{\numberline {3.3}Pseudorange Measurements}{35}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Raw measurements computation}{35}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}Pseudorange correction and PVT}{37}{subsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.3.3}GNSS software receivers}{37}{subsection.3.3.3}% 
\contentsline {section}{\numberline {3.4}Networked \ac {GNSS} Mass-market receiver: a concept}{39}{section.3.4}% 
\contentsline {chapter}{\numberline {4}GNSS-based collaborative ranging algorithms}{41}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Baseline: Fundamental Definitions}{41}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}Sensor-based baseline estimation}{42}{subsection.4.1.1}% 
\contentsline {section}{\numberline {4.2}Absolute Positions Distance (APD)}{43}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}Error modelling of Euclidean distance in {GNSS} positioning solutions}{44}{subsection.4.2.1}% 
\contentsline {subsubsection}{Distribution of GNSS positioning solutions}{45}{subsection.4.2.1}% 
\contentsline {subsubsection}{On the generalization of \ac {APD} error distribution}{46}{figure.caption.30}% 
\contentsline {section}{\numberline {4.3}GNSS-based differential baseline computation}{47}{section.4.3}% 
\contentsline {subsection}{\numberline {4.3.1}GNSS-based baseline estimation}{48}{subsection.4.3.1}% 
\contentsline {subsection}{\numberline {4.3.2}Time-compensation of asynchronous observables (Doppler-based)}{49}{subsection.4.3.2}% 
\contentsline {subsection}{\numberline {4.3.3}Raw Pseudorange Ranging (PR)}{49}{subsection.4.3.3}% 
\contentsline {subsection}{\numberline {4.3.4}Single Difference Ranging (SD)}{50}{subsection.4.3.4}% 
\contentsline {subsection}{\numberline {4.3.5}Double Difference Ranging (DD)}{51}{subsection.4.3.5}% 
\contentsline {subsubsection}{Timing of observables exchange through packet network}{53}{equation.4.3.27}% 
\contentsline {section}{\numberline {4.4}Inter-agent Ranging (IAR)}{54}{section.4.4}% 
\contentsline {subsection}{\numberline {4.4.1}Theoretical Inter Agent Range}{54}{subsection.4.4.1}% 
\contentsline {subsection}{\numberline {4.4.2}Numerically-stable Law of Cosines for \ac {GNSS} applications}{56}{subsection.4.4.2}% 
\contentsline {subsection}{\numberline {4.4.3}Time-compensation of asynchronous observables (orbit-based)}{57}{subsection.4.4.3}% 
\contentsline {subsection}{\numberline {4.4.4}Mean and variance analytical derivation}{58}{subsection.4.4.4}% 
\contentsline {section}{\numberline {4.5}Final remarks}{60}{section.4.5}% 
\contentsline {chapter}{\numberline {5}Simulation analysis and assessment of GNSS-based baseline length computation}{63}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Analysis and assessment of \ac {IAR} model}{64}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Geometrical Model}{64}{subsection.5.1.1}% 
\contentsline {subsection}{\numberline {5.1.2}Variance behaviour in single satellite \ac {IAR}}{65}{subsection.5.1.2}% 
\contentsline {subsection}{\numberline {5.1.3}Analytic formula assessment }{66}{subsection.5.1.3}% 
\contentsline {subsection}{\numberline {5.1.4}Experimental framework in a controlled static environment}{67}{subsection.5.1.4}% 
\contentsline {subsection}{\numberline {5.1.5}Experimental assessment using COTS GNSS Receivers}{70}{subsection.5.1.5}% 
\contentsline {subsubsection}{Validation of the theoretical model using COTS receiver}{71}{table.caption.52}% 
\contentsline {subsection}{\numberline {5.1.6}Weighted \ac {IAR} measurements}{73}{subsection.5.1.6}% 
\contentsline {section}{\numberline {5.2}Statistical analysis of baseline estimation techniques}{73}{section.5.2}% 
\contentsline {subsubsection}{MATLAB Simulation Environment}{73}{figure.caption.55}% 
\contentsline {subsection}{\numberline {5.2.1}Cross correlation among standalone GNSS measurements and collaborative baseline length estimation}{74}{subsection.5.2.1}% 
\contentsline {subsection}{\numberline {5.2.2}Statistical Distribution of the Baseline Length Error considering independent Gaussian inputs}{78}{subsection.5.2.2}% 
\contentsline {section}{\numberline {5.3}Final remarks}{82}{section.5.3}% 
\contentsline {chapter}{\numberline {6}Hybridized Navigation Filters and theoretical limits on positioning with correlated measurements}{83}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Modified \ac {HMM} for correlated measurements}{84}{section.6.1}% 
\contentsline {subsection}{\numberline {6.1.1}A model of the positioning problem: the \ac {HMM}}{84}{subsection.6.1.1}% 
\contentsline {subsection}{\numberline {6.1.2}Combined Observable Measurements and Implicit Belief Propagation}{86}{subsection.6.1.2}% 
\contentsline {subsection}{\numberline {6.1.3}Hybrid Measurements Modelling in \ac {GNSS}}{87}{subsection.6.1.3}% 
\contentsline {section}{\numberline {6.2}Hybrid \ac {WLS}}{88}{section.6.2}% 
\contentsline {subsection}{\numberline {6.2.1}Weighted \ac {SAIA} for robust measurement integration in \ac {WLS}}{89}{subsection.6.2.1}% 
\contentsline {section}{\numberline {6.3}Approximated Bayesian Estimation}{90}{section.6.3}% 
\contentsline {subsection}{\numberline {6.3.1}Hybrid Extended Kalman Filter}{91}{subsection.6.3.1}% 
\contentsline {subsubsection}{Models equations}{91}{subsection.6.3.1}% 
\contentsline {subsubsection}{EKF routine}{92}{equation.6.3.36}% 
\contentsline {subsubsection}{Error-state EKF indirect routine}{93}{equation.6.3.43}% 
\contentsline {subsection}{\numberline {6.3.2}Hybrid Particle Filter}{94}{subsection.6.3.2}% 
\contentsline {subsubsection}{Model Equations}{95}{subsection.6.3.2}% 
\contentsline {section}{\numberline {6.4}On the approximation of a \ac {CRLB} for hybrid navigation filters}{96}{section.6.4}% 
\contentsline {subsection}{\numberline {6.4.1}\ac {FIM} and \ac {CRLB}: definitions}{97}{subsection.6.4.1}% 
\contentsline {subsection}{\numberline {6.4.2}Range Contributions Modelling}{97}{subsection.6.4.2}% 
\contentsline {section}{\numberline {6.5}Fisher Information Matrix in Positioning Estimation}{98}{section.6.5}% 
\contentsline {subsection}{\numberline {6.5.1}Fisher Information Matrix for Satellite-only Contributions}{98}{subsection.6.5.1}% 
\contentsline {subsection}{\numberline {6.5.2}Fisher Information Matrix for Cooperative Contributions}{99}{subsection.6.5.2}% 
\contentsline {subsection}{\numberline {6.5.3}\ac {FIM} computation in non-linear system estimation}{100}{subsection.6.5.3}% 
\contentsline {subsection}{\numberline {6.5.4}On the Approximation of the \ac {FIM} for Hybrid Navigation Filters}{100}{subsection.6.5.4}% 
\contentsline {chapter}{\numberline {7}GNSS-based Cooperative Positioning Implementation and Performance Analysis}{103}{chapter.7}% 
\contentsline {section}{\numberline {7.1}Multiagent collaborative IAR measurements for compensation of GNSS outages}{103}{section.7.1}% 
\contentsline {subsection}{\numberline {7.1.1}Pseudo-IAR-based Robust Collaborative Algorithm}{104}{subsection.7.1.1}% 
\contentsline {subsubsection}{Kalman state prediction as a-priori state estimate}{104}{subsection.7.1.1}% 
\contentsline {subsubsection}{Displacement vector and pseudo-position as a virtual landmark}{105}{figure.caption.70}% 
\contentsline {subsubsection}{CP algorithm implementation and simulation parameters}{105}{equation.7.1.2}% 
\contentsline {subsection}{\numberline {7.1.2}Numerical Results}{106}{subsection.7.1.2}% 
\contentsline {subsection}{\numberline {7.1.3}Remarks on \ac {H-LMS} smoothed solution}{107}{subsection.7.1.3}% 
\contentsline {section}{\numberline {7.2}Precision GNSS and Collaborative Relative Ranges Integration}{107}{section.7.2}% 
\contentsline {subsection}{\numberline {7.2.1}Methodology}{108}{subsection.7.2.1}% 
\contentsline {subsection}{\numberline {7.2.2}H-LMS Positioning on Bernoullian Trajectory}{110}{subsection.7.2.2}% 
\contentsline {subsubsection}{Statistics of collaborative IAR measurements}{110}{figure.caption.75}% 
\contentsline {subsubsection}{On the comparison of theoretical and experimental limits}{111}{figure.caption.77}% 
\contentsline {subsection}{\numberline {7.2.3}H-LMS Estimation on Other Trajectories}{115}{subsection.7.2.3}% 
\contentsline {subsection}{\numberline {7.2.4}H-EKF Estimation on a Bernoullian Trajectory}{116}{subsection.7.2.4}% 
\contentsline {subsection}{\numberline {7.2.5}Remarks on hybrid positioning solution}{117}{subsection.7.2.5}% 
\contentsline {section}{\numberline {7.3}Accuracy Improvement of Position Estimation through \ac {GNSS}-only Collaborative Navigation Systems}{118}{section.7.3}% 
\contentsline {subsection}{\numberline {7.3.1}Simulated scenario and experimental setup}{118}{subsection.7.3.1}% 
\contentsline {subsection}{\numberline {7.3.2}Scenario generation}{119}{subsection.7.3.2}% 
\contentsline {subsection}{\numberline {7.3.3}Epochs misalignment}{121}{subsection.7.3.3}% 
\contentsline {subsection}{\numberline {7.3.4}Results}{121}{subsection.7.3.4}% 
\contentsline {subsubsection}{Performance metrics for positioning assessment}{122}{figure.caption.95}% 
\contentsline {subsubsection}{Experimental results}{123}{equation.7.3.9}% 
\contentsline {subsubsection}{High relative dynamics}{124}{equation.7.3.9}% 
\contentsline {subsubsection}{Low relative dynamics}{124}{equation.7.3.9}% 
\contentsline {subsection}{\numberline {7.3.5}Data fitting of Mean Aggregated Improvement}{125}{subsection.7.3.5}% 
\contentsline {subsection}{\numberline {7.3.6}Remarks on the accuracy improvement}{125}{subsection.7.3.6}% 
\contentsline {section}{\numberline {7.4}Assessment of the computational complexity of Bayesian positioning estimation in EKF and PF}{126}{section.7.4}% 
\contentsline {subsubsection}{Suboptimal implementation of a Particle Filter (s-PF)}{126}{section.7.4}% 
\contentsline {subsection}{\numberline {7.4.1}Computational complexity of Bayesian Estimation}{127}{subsection.7.4.1}% 
\contentsline {subsection}{\numberline {7.4.2}Test Scenario}{127}{subsection.7.4.2}% 
\contentsline {subsection}{\numberline {7.4.3}Final remarks}{129}{subsection.7.4.3}% 
\contentsline {chapter}{\numberline {8}Implementation of a Proof Of Concept}{131}{chapter.8}% 
\contentsline {section}{\numberline {8.1}The role of \ac {GNSS} in smart connected environments}{131}{section.8.1}% 
\contentsline {subsection}{\numberline {8.1.1}\ac {GNSS} advances in mass-market smart devices}{132}{subsection.8.1.1}% 
\contentsline {subsection}{\numberline {8.1.2}The HANSEL project}{133}{subsection.8.1.2}% 
\contentsline {section}{\numberline {8.2}From smartphones to networked \ac {GNSS} receivers}{134}{section.8.2}% 
\contentsline {subsection}{\numberline {8.2.1}Hardware selection: Xiaomi Mi 8 Pro and Braoadcom BCM47755}{136}{subsection.8.2.1}% 
\contentsline {section}{\numberline {8.3}CAPS.loc: Framework overview}{137}{section.8.3}% 
\contentsline {subsection}{\numberline {8.3.1}Collaborative Positioning Service}{137}{subsection.8.3.1}% 
\contentsline {subsubsection}{Raw Measurements Database (db)}{138}{subsection.8.3.1}% 
\contentsline {subsubsection}{Cooperative Positioning System API}{139}{subsection.8.3.1}% 
\contentsline {subsection}{\numberline {8.3.2}Collaborative Positioning Application - CPA}{140}{subsection.8.3.2}% 
\contentsline {subsubsection}{Main activity}{140}{figure.caption.114}% 
\contentsline {subsubsection}{Agent Subscriber}{140}{figure.caption.114}% 
\contentsline {subsubsection}{GNSS Container}{141}{figure.caption.115}% 
\contentsline {subsubsection}{Agent Handler}{142}{figure.caption.115}% 
\contentsline {subsubsection}{CRMcollector}{143}{figure.caption.115}% 
\contentsline {subsubsection}{GNSSMeasScript}{143}{figure.caption.115}% 
\contentsline {subsubsection}{Graphic User Interface and Settings}{144}{Item.9}% 
\contentsline {section}{\numberline {8.4}Offline asynchronous measurements combination}{147}{section.8.4}% 
\contentsline {subsection}{\numberline {8.4.1}Baseline length estimation through Android GNSS raw measurements}{147}{subsection.8.4.1}% 
\contentsline {subsubsection}{Static non-zero-baseline test}{147}{subsection.8.4.1}% 
\contentsline {section}{\numberline {8.5}Real-time CAPS.loc experiments}{149}{section.8.5}% 
\contentsline {subsection}{\numberline {8.5.1}Setup}{150}{subsection.8.5.1}% 
\contentsline {subsection}{\numberline {8.5.2}Performance metrics}{150}{subsection.8.5.2}% 
\contentsline {subsection}{\numberline {8.5.3}Test Scenarios}{154}{subsection.8.5.3}% 
\contentsline {subsubsection}{Agents dynamics}{154}{subsection.8.5.3}% 
\contentsline {subsubsection}{Baseline length}{155}{figure.caption.127}% 
\contentsline {section}{\numberline {8.6}Results}{155}{section.8.6}% 
\contentsline {subsubsection}{Experiment f-01 16102019-01-A: Static receivers at zero-baseline}{156}{section.8.6}% 
\contentsline {subsubsection}{Experiment d-02 11102019-02-B: side-by-side pedestrian walk with variable speed and baseline}{157}{figure.caption.130}% 
\contentsline {subsubsection}{Experiment g-05 30102019-05-D: Vehicular (Bicycle) and pedestrian dynamics with variable speed and baseline}{157}{figure.caption.131}% 
\contentsline {subsubsection}{Experiment c-01 04102019-01-C: Pedestrian relative dynamics with variable speed, baseline and altitude}{158}{figure.caption.132}% 
\contentsline {subsection}{\numberline {8.6.1}Summary of experimental results and general comments}{160}{subsection.8.6.1}% 
\contentsline {subsubsection}{Availability}{160}{table.caption.138}% 
\contentsline {subsubsection}{Expected improvement in real scenarios}{164}{table.caption.138}% 
\contentsline {section}{\numberline {8.7}Final remarks and further works}{164}{section.8.7}% 
\contentsline {chapter}{\numberline {9}Conclusions}{167}{chapter.9}% 
\contentsline {section}{\numberline {9.1}Further works}{168}{section.9.1}% 
\contentsline {chapter}{\numberline {A}Fundamentals on Reference Systems and Frames}{169}{appendix.A}% 
\contentsline {section}{\numberline {A.1}Conventional Celestial Reference System CRS}{169}{section.A.1}% 
\contentsline {section}{\numberline {A.2}Conventional Terrestrial Reference System (TRS)}{169}{section.A.2}% 
\contentsline {section}{\numberline {A.3}Conversion between reference systems}{171}{section.A.3}% 
\contentsline {paragraph}{Ellipsoidal Coordinates ($\phi $, $\lambda $, $h$) }{171}{section.A.3}% 
\contentsline {paragraph}{Conversion between ECEF to Local Tangent Plane (LTP) Coordinates }{172}{equation.A.3.3}% 
\contentsline {subsection}{\numberline {A.3.1}Range, Elevation and Azimuth computation from ENU frame}{172}{subsection.A.3.1}% 
\contentsline {chapter}{\numberline {B}Position Error Covariance Matrix and Geometrical representations}{175}{appendix.B}% 
\contentsline {section}{\numberline {B.1}Sample Covariance Matrix estimation}{175}{section.B.1}% 
\contentsline {section}{\numberline {B.2}Covariance Matrix and Error Ellipse }{176}{section.B.2}% 
\contentsline {subsection}{\numberline {B.2.1}Eigendecomposition of Covariance}{178}{subsection.B.2.1}% 
\contentsline {subsection}{\numberline {B.2.2}Covariance conversion between reference frames}{179}{subsection.B.2.2}% 
\contentsline {chapter}{\numberline {C}CPS message fields}{181}{appendix.C}% 
\contentsline {section}{\numberline {C.1}Data fields description}{181}{section.C.1}% 
\contentsline {section}{\numberline {C.2}APIs}{182}{section.C.2}% 
\contentsline {subsubsection}{Registration/Unregistration of a the agents}{182}{section.C.2}% 
\contentsline {subsubsection}{Database Upkeep API}{183}{table.caption.149}% 
\contentsline {subsubsection}{Raw measurements and status}{183}{table.caption.150}% 
\contentsline {subsubsection}{Visibility configuration}{184}{lstnumber.C.1.16}% 
\contentsline {subsubsection}{Registered agents}{185}{lstnumber.C.2.4}% 
\contentsline {subsubsection}{Raw measurements upload}{185}{lstnumber.C.3.3}% 
\contentsline {subsubsection}{Raw measurements download}{186}{lstnumber.C.4.22}% 
\contentsline {subsubsection}{Database Download API}{187}{lstnumber.C.5.11}% 
\contentsline {chapter}{\numberline {D}Android Location Services}{189}{appendix.D}% 
\contentsline {section}{\numberline {D.1}Background software architecture: Positioning in Android OS}{189}{section.D.1}% 
\contentsline {subsection}{\numberline {D.1.1}Android Location Manager}{189}{subsection.D.1.1}% 
\contentsline {subsection}{\numberline {D.1.2}Raw GNSS measurements}{190}{subsection.D.1.2}% 
\contentsline {subsubsection}{Fused Location Provider API}{191}{figure.caption.156}% 
\contentsline {chapter}{Bibliography}{199}{appendix*.158}% 
